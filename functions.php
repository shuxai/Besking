<?php 
if (!defined('__TYPECHO_ROOT_DIR__')) exit;

/* 后台设置 */
function themeConfig($form) {
    //站点logo地址
    $logoUrl = new Typecho_Widget_Helper_Form_Element_Text('logoUrl', NULL, NULL, _t('站点LOGO地址'), _t('在这里填入一个图片URL地址, 以在网站标题前加上一个LOGO'));
    $form->addInput($logoUrl->addRule('xssCheck', _t('请不要在图片链接中使用特殊字符')));

    //文章打赏支付宝图片地址
    $alipayUrl = new Typecho_Widget_Helper_Form_Element_Text('alipayUrl', NULL, NULL, _t('支付宝打赏地址'), _t('在这里填入一个图片URL地址, 以在文章支付宝打赏地址'));
    $form->addInput($alipayUrl->addRule('xssCheck', _t('请不要在图片链接中使用特殊字符')));

    //文章打赏支付宝图片地址
    $weixinUrl = new Typecho_Widget_Helper_Form_Element_Text('weixinUrl', NULL, NULL, _t('微信打赏地址'), _t('在这里填入一个图片URL地址, 以在文章微信打赏地址'));
    $form->addInput($weixinUrl->addRule('xssCheck', _t('请不要在图片链接中使用特殊字符')));

    //关于我界面图片地址
    $aboutabotUrl = new Typecho_Widget_Helper_Form_Element_Text('aboutabotUrl', NULL, NULL, _t('关于我地址'), _t('在这里填入一个图片URL地址, 以在关于我地址'));
    $form->addInput($aboutabotUrl->addRule('xssCheck', _t('请不要在图片链接中使用特殊字符')));

    //关于我微信图片地址
    $aboutweixinUrl = new Typecho_Widget_Helper_Form_Element_Text('aboutweixinUrl', NULL, NULL, _t('微信地址'), _t('在这里填入一个图片URL地址, 以在关于我微信地址'));
    $form->addInput($aboutweixinUrl->addRule('xssCheck', _t('请不要在图片链接中使用特殊字符')));

    //关于个性签名地址
    $autographUrl = new Typecho_Widget_Helper_Form_Element_Text('autographUrl', NULL, NULL, _t('个性签名地址'), _t('填写一个个性签名'));
    $form->addInput($autographUrl->addRule('xssCheck', _t('请直接输入个性签名')));

    //底部github地址
    $gooterGithub = new Typecho_Widget_Helper_Form_Element_Text('gooterGithub', NULL, NULL, _t('用户网站底部github地址'), _t('填写一个个性签名'));
    $form->addInput($gooterGithub->addRule('xssCheck', _t('请不要在图片链接中使用特殊字符')));

    //底部qq地址
    $gooterQQ = new Typecho_Widget_Helper_Form_Element_Text('gooterQQ', NULL, NULL, _t('用户网站底部qq地址，请直接输入QQ号'), _t('填写一个个性签名'));
    $form->addInput($gooterQQ->addRule('xssCheck', _t('请不要在图片链接中使用特殊字符')));

    //底部邮箱地址
    $gooterEmail = new Typecho_Widget_Helper_Form_Element_Text('gooterEmail', NULL, NULL, _t('用户网站底部邮箱地址，请直接输入邮箱'), _t('填写一个个性签名'));
    $form->addInput($gooterEmail->addRule('xssCheck', _t('请不要在图片链接中使用特殊字符')));

    //底部gitee地址
    $gooterGitee = new Typecho_Widget_Helper_Form_Element_Text('gooterGitee', NULL, NULL, _t('用户网站底部gitee地址'), _t('填写一个个性签名'));
    $form->addInput($gooterGitee->addRule('xssCheck', _t('请不要在图片链接中使用特殊字符')));
    
    //侧边栏
    $sidebarBlock = new Typecho_Widget_Helper_Form_Element_Checkbox('sidebarBlock', 
    array(
    'ShowCategory' => _t('全部分类'),
    'ShowRecentPosts' => _t('最新文章'),
    'ShowRecentComments' => _t('最新评论'),
    'ShowTags' => _t('标签云')),
    array('ShowCategory', 'ShowRecentPosts', 'ShowRecentComments', 'ShowTags'), _t('侧边栏显示'));    
    $form->addInput($sidebarBlock->multiMode());


    //统计代码
    $GoogleAnalytics = new Typecho_Widget_Helper_Form_Element_Textarea('GoogleAnalytics', NULL, NULL, _t('统计代码'), _t('填写你的各种跟踪统计代码，相当于页尾代码'));
    $form->addInput($GoogleAnalytics);
    

    

}


//缩略图调用
function showThumb($obj,$size=null,$link=false){
    preg_match_all( "/<[images|IMG].*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>/", $obj->content, $matches );
    $thumb = '';
    $options = Typecho_Widget::widget('Widget_Options');
    $attach = $obj->attachments(1)->attachment;
    if (isset($attach->isImage) && $attach->isImage == 1){
        $thumb = $attach->url;
        if(!empty($options->src_add) && !empty($options->cdn_add)){
            $thumb = str_ireplace($options->src_add,$options->cdn_add,$thumb);
        }
    }elseif(isset($matches[1][0])){
        $thumb = $matches[1][0];
        if(!empty($options->src_add) && !empty($options->cdn_add)){
            $thumb = str_ireplace($options->src_add,$options->cdn_add,$thumb);
        }
    }
    if(empty($thumb) && empty($options->default_thumb)){
		$thumb= $options->themeUrl .'/images/thumb/' . rand(1, 20) . '.jpg';
        return $thumb;
    }else{
        $thumb = empty($thumb) ? $options->default_thumb : $thumb;
    }
    if($link){
        return $thumb;
    }
}


/*文章阅读次数统计*/
function get_post_view($archive) {
    $cid = $archive->cid;
    $db = Typecho_Db::get();
    $prefix = $db->getPrefix();
    if (!array_key_exists('views', $db->fetchRow($db->select()->from('table.contents')))) {
        $db->query('ALTER TABLE `' . $prefix . 'contents` ADD `views` INT(10) DEFAULT 0;');
        echo 0;
        return;
    }
    $row = $db->fetchRow($db->select('views')->from('table.contents')->where('cid = ?', $cid));
    if ($archive->is('single')) {
        $views = Typecho_Cookie::get('extend_contents_views');
        if (empty($views)) {
            $views = array();
        } else {
            $views = explode(',', $views);
        }
        if (!in_array($cid, $views)) {
            $db->query($db->update('table.contents')->rows(array('views' => (int)$row['views'] + 1))->where('cid = ?', $cid));
            array_push($views, $cid);
            $views = implode(',', $views);
            Typecho_Cookie::set('extend_contents_views', $views); //记录查看cookie
            
        }
    }
    echo $row['views'];
}

//文章导航fa图标
function fa_ico() {
    $options = Typecho_Widget::widget('Widget_Options');
    if (!empty($options->fatext)) {
        $text = $options->fatext;
    }else{
        $text="<i class=\"fa fa-plug\"></i>\n<i class=\"fa fa-tablet\"></i>\n<i class=\"fa fa-bug\"></i>\n<i class=\"fa fa-envelope-square\"></i>\n<i class=\"fa fa-gift\"></i>\n<i class=\"fa fa-database\"></i>\n<i class=\"fa fa-book\"></i>";
    }
    $b_arr = explode("\n", $text);
        
    return $b_arr;
}
