<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
	<aside class="sidebar">
	<div class="blog_classify">
		<ul>
			<?php if (!empty($this->options->sidebarBlock) && in_array('ShowCategory', $this->options->sidebarBlock)): ?>
			<li id="classifyBox" class="wow flipInY mobile_close">
				<div class="searchBox">
				<form id="search" method="post" action="<?php $this->options->siteUrl(); ?>" role="search">
					<input type="text" id="s" name="s" class="text" placeholder="<?php _e('输入关键字搜索'); ?>" />
				<a onclick="document:search.button()"><i class="fa fa-search"></i> </a>
                </form>				
				</div>
				<span class="classify_title" id="allClassify">全部分类</span>
				<ul class="classify" id="classify">
					 <?php $this->widget('Widget_Metas_Category_List')->parse('<li><a href="{permalink}">{name}</a> ({count})</li>'); ?>
				</ul>
			</li>
			<?php endif; ?>
			<?php if (!empty($this->options->sidebarBlock) && in_array('ShowRecentPosts', $this->options->sidebarBlock)): ?>
			<li class="wow fadeInRight mobile_close" name="hot_article">
				<span class="classify_title">最新文章</span>
				<ul class="hot_article" id="hot_article">
					<?php $this->widget('Widget_Contents_Post_Recent','pageSize=10')->to($post); ?>
						<?php while($post->next()): ?>
							<li><a href="<?php $post->permalink(); ?>" title="<?php $post->title(); ?>"><?php $post->title(); ?></a></li>
					<?php endwhile; ?>
				</ul>
			</li>
			<?php endif; ?>
			<?php if (!empty($this->options->sidebarBlock) && in_array('ShowRecentComments', $this->options->sidebarBlock)): ?>
			<li class="wow rotateInDownRight mobile_close" name="top_article">
				<span class="classify_title">
					回复列表
				</span>
				<ul class="hot_article" id="top_article">
				<?php $this->widget('Widget_Comments_Recent','pageSize=5')->to($comments); ?>
    				<?php while($comments->next()): ?>
        			<li><a href="<?php $comments->permalink(); ?>"><?php $comments->excerpt(10, '[...]'); ?></a></li>
    				<?php endwhile; ?>
				</ul>
			</li>
			<?php endif; ?>

			<?php if (!empty($this->options->sidebarBlock) && in_array('ShowTags', $this->options->sidebarBlock)): ?>
			<li class="wow rotateInDownRight mobile_close" name="top_article">
				<span class="classify_title">
					热门标签
				</span>
				<ul class="hot_article" id="top_article">
				<?php $this->widget('Widget_Metas_Tag_Cloud', 'ignoreZeroCount=1&limit=30')->to($tags); ?>
    				<?php while($tags->next()): ?>
        			<li><a style="color: rgb(<?php echo(rand(0, 255)); ?>, <?php echo(rand(0,255)); ?>, <?php echo(rand(0, 255)); ?>)" href="<?php $tags->permalink(); ?>" title='<?php $tags->name(); ?>'><?php $tags->name(); ?></a></li>
    				<?php endwhile; ?>
				</ul>
			</li>
			<?php endif; ?>
		</ul>
	</div>
	</aside><!-- end #sidebar -->