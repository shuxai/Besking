<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>

<div class="w700">
	<div id="detail" class="wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
	<!--标题-->
	<h3 class="article_title"><?php $this->title() ?></h3>
	<p class="wow fadeInUp article_label animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
		<?php _e(''); ?><?php $this->tags(', ', true, ''); ?>
	</p>
	<div class="detail_info">
		<i class="fa fa-user-o"></i>
		<span><?php $this->author(); ?></span>
		<i class="fa fa-clock-o"></i>
		<span><?php $this->date('Y-m-d'); ?></span>
		<i class="fa fa-eye"></i>
		<span><?php get_post_view($this) ?></span>
		<i class="fa fa-comment"></i>
		<span><?php $this->commentsNum('0', '1', '%d'); ?></span>
	</div>
	<div class="detail_content">
		<div class="post-content" itemprop="articleBody">
            <?php $this->content(); ?>
        </div>
	</div>

	</div>

	<!--发表评论-->
	<?php $this->need('comments.php'); ?>


</div>

<?php $this->need('footer.php'); ?>