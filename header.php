<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="<?php $this->options->charset(); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php $this->archiveTitle(array(
            'category'  =>  _t('分类 %s 下的文章'),
            'search'    =>  _t('包含关键字 %s 的文章'),
            'tag'       =>  _t('标签 %s 下的文章'),
            'author'    =>  _t('%s 发布的文章')
        ), '', ' - '); ?><?php $this->options->title(); ?></title>
    <!--引入CSS-->
    <link rel="stylesheet" href="<?php $this->options->themeUrl('css/libs/animate.min.css'); ?>">
    <link rel="stylesheet" href="<?php $this->options->themeUrl('font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php $this->options->themeUrl('css/base.css'); ?>">
    <link rel="stylesheet" href="<?php $this->options->themeUrl('css/article.css'); ?>">
    <link rel="stylesheet" href="<?php $this->options->themeUrl('css/detail.min.css'); ?>">
    <link rel="stylesheet" href="<?php $this->options->themeUrl('css/prettify.min.css'); ?>">
    <link rel="stylesheet" href="<?php $this->options->themeUrl('css/about.css'); ?>">
    <link rel="shortcut icon" href="<?php $this->options->themeUrl('images/favicon.png'); ?>">

    <link href="//cdn.bootcss.com/highlight.js/9.1.0/styles/monokai-sublime.min.css" rel="stylesheet">
    <script src="//cdn.bootcss.com/highlight.js/9.1.0/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>

<!--头部开始-->
<div id="header">
    <menu class="menu_close" id="menu">
        <span></span>
        <span></span>
        <span></span>
    </menu>
    <nav hidden="hidden" id="nav" class="wow bounceInRight">
    <div class="nav">
        <div class="nav-logo">
            <a href="<?php $this->options->siteUrl(); ?>">
                <?php if (!empty($this->options->logoUrl)): ?>
                <img src="<?php $this->options->logoUrl(); ?>" alt="<?php $this->options->title();?>" />
                <?php else: ?>
                <img src="<?php $this->options->themeUrl('images/logo.png'); ?>" alt="<?php $this->options->title();?>">
                <?php endif; ?>
            <h1><?php $this->options->title() ?></h1>
            </a>
        </div>
        <div class="nav-content">
            <ul style="color: #fff;">
                <li ><i class="fa fa-home""></i><a href="<?php $this->options->siteUrl(); ?>">首页</a></li>
                  <?php 
                  $this->widget('Widget_Metas_Category_List')->to($cats); 
                  $i=0;  $b_arr = fa_ico(); 
                  ?>
                  <?php while ($cats->next()): ?>
                  <li>
                    <a href="<?php $cats->permalink()?>">
                      <?php echo $b_arr[$i]; ?> 
                      <?php $cats->name()?></a>
                  </li>
                  <?php $i++; ?>
                  <?php endwhile; ?>
                <?php $this->widget('Widget_Contents_Page_List')
               ->parse('<li><i class="fa fa-envelope"></i><a href="{permalink}">{title}</a></li>'); ?>
            </ul>
        </div>
    </div>
    </nav>
</div>
<!--头部结束-->
