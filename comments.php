<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
	<!--发表评论-->
	<?php if($this->allow('comment')): ?>
	<div class="wow fadeInUp detail_comment" data-wow-delay=".5s">
		<h3>发表评论</h3>
    <!-- 输入表单开始 -->
    <form method="post" action="<?php $this->commentUrl() ?>" id="comment-form" role="form">
	        <!-- 如果当前用户已经登录 -->
	        <?php if($this->user->hasLogin()): ?>
	            <!-- 显示当前登录用户的用户名以及登出连接 -->
	            <p style="padding-bottom: 5px;">登录身份:<a href="<?php $this->options->adminUrl(); ?>"><?php $this->user->screenName(); ?></a>. 
	            <a href="<?php $this->options->logoutUrl(); ?>" title="Logout">退出&raquo;</a></p>
	
	        <!-- 若当前用户未登录 -->
	        <?php else: ?>
	            <!-- 要求输入名字、邮箱、网址 -->
		    <p><input type="text" name="author" class="text" size="35" value="<?php $this->remember('author'); ?>" required placeholder="请输入您的昵称……"/><label>昵称*</label></p>
		    <p><input type="text" name="mail" class="text" size="35" value="<?php $this->remember('mail'); ?>" required placeholder="请输入您的邮箱……"/><label>邮箱*</label></p>
		    <p><input type="text" name="url"  class="text" size="35" value="<?php $this->remember('url'); ?>" placeholder="请输入您的网站……"/><label>网站</label></p>
	        <?php endif; ?>
	        <!-- 输入要回复的内容 -->

		 <p><input type="text" name="text" class="text" size="35" value="<?php $this->remember('text'); ?>" placeholder="随便说两句吧……"/><label>内容</label></p>
		 <p><input type="submit" value="提交评论" class="submitComment" /></p>
	    </form>
	</div>
    <?php else: ?>

    <h3><?php _e('评论已关闭'); ?></h3>
    <?php endif; ?>
	<!--评论列表-->
	<?php $this->comments()->to($comments); ?>
    <?php if ($comments->have()): ?>
	<div class="article_comment">
		<ul id="commentBox">
			<?php $this->comments()->to($comments); ?>
        	<?php while($comments->next()): ?>
			<li name="<?php $comments->theId(); ?>" class="wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
				<div class="commentHeadImg">
					<img src="https://www.hiai.top/yuan/logo-1.png" alt="">
				</div>
				<div class="comment"><h3><?php $comments->author(); ?></h3><span><p>
					<?php $comments->content(); ?>
				<p>
					<span><?php $comments->date('Y-m-d'); ?> <?php $comments->date('h:i a'); ?></span>
					<span class="reply" name="<?php $comments->theId(); ?>">回复</span>
				</p>
				<div name="replyBox"></div>
				</div>
			</li>
			<?php endwhile; ?>
		</ul>
	</div>
	<?php endif; ?>