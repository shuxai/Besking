
$(function () {
    console.log("如果你喜欢本站模板,欢迎发邮件给783115652@qq.com,免费分享本站静态模板哦~");
    console.log("欢迎加群进行学习交流哦~(211758229)");
    // 刷新回到顶部 防止wow.js的动画失效
    $('body,html').animate({scrollTop: 0},700);
    // 初始化wow
    new WOW().init();
    let menuId = $('#menu');
    let nav = $('#nav');

    // menu点击事件
    menuId.click(function () {
        if (menuId.hasClass('menu_close')){
            nav.removeClass('bounceOutRight');
            nav.removeAttr('style');
            nav.addClass('bounceInRight');
            menuId.removeClass('menu_close');
            menuId.addClass('menu_open');
            nav.removeAttr('hidden');
        }else {
            nav.removeClass('bounceInRight');
            nav.removeAttr('style');
            nav.addClass('bounceOutRight');
            menuId.removeClass('menu_open');
            menuId.addClass('menu_close');
        }
    });

    // 鼠标点击事件
    var i = 0;
    $('html').click(function (element) {
        let arr = ["富强","民主","文明","和谐","自由","平等","公正","法治","爱国","敬业","诚信","友善"];
        let $i = $("<span/>").text(arr[i]);
        i = (i+1) % arr.length;
        let x = element.pageX;
        let y = element.pageY;
        $i.css({
            "z-index": 99999,
            "top": y - 20,
            "left": x,
            "position": "absolute",
            "font-weight": "bold",
            "color": "#ff6651"
        });
        $('body').append($i);
        $i.animate({
            "top": y-180,
            "opacity":0
        },1500,function () {
            $i.remove();
        });
    });
    // toTop事件
    $('#toTop').click(function () {
        $('html,body').animate({scrollTop:0},500);
    });

    // 上一次滚轮的位置
    var last_position = 0;
    // 监听滚轮事件
    $(window).scroll(function () {
        let offset = 500;
        let pageY = $(window).scrollTop();
        pageY > offset ? $('#toTop').removeAttr('hidden') : $('#toTop').attr('hidden','hidden');
        if (pageY > 1000){
            $("#classifyBox").removeAttr('style');
            $("#classifyBox").addClass('box_bounceInDown_fixed');
        }else {
            $("#classifyBox").removeClass('box_bounceInDown_fixed');
        }
        if (pageY>600){
            $('#message_search').removeAttr('hidden','hidden');
            $("#message_search").removeAttr('style');
            $("#message_search").addClass('box_bounceInUp_fixed');
        }else{
            $('#message_search').attr('hidden','hidden');
            $("#message_search").removeClass('box_bounceInUp_fixed');
        }
        // 获取当前位置
        let position = window.scrollY;
        if(position - last_position > 0) {
            if (menuId.hasClass("menu_open")) {
                nav.removeClass('bounceInRight');
                nav.removeAttr('style');
                nav.addClass('bounceOutRight');
                menuId.removeClass('menu_open');
                menuId.addClass('menu_close');
            }
        }
        last_position = position;
    });
});