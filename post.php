<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>


<div class="w700">
	<div id="detail" class="wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
	<!--标题-->
	<h3 class="article_title"><?php $this->title() ?></h3>
	<p class="wow fadeInUp article_label animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
		<?php _e(''); ?><?php $this->tags(', ', true, ''); ?>
	</p>
	<div class="detail_info">
		<i class="fa fa-user-o"></i>
		<span><?php $this->author(); ?></span>
		<i class="fa fa-clock-o"></i>
		<span><?php $this->date('Y-m-d'); ?></span>
		<i class="fa fa-eye"></i>
		<span><?php get_post_view($this) ?></span>
		<i class="fa fa-comment"></i>
		<span><?php $this->commentsNum('0', '1', '%d'); ?></span>
	</div>
	<div class="detail_content">
		<div class="post-content" itemprop="articleBody">
            <?php $this->content(); ?>
        </div>
		<div class="prise">
			<div class="like">
				<span class="likeHeart"></span>
				<span>5</span>人点赞
			</div>
			<div class="pay">
				<div class="priseImg">
					<div class="weChatPay">
						<p>微信打赏</p>
						<a href="https://www.hiai.top/yuan/weixinpay.jpg"data-fancybox="images">
							<?php if (!empty($this->options->weixinUrl)): ?>
                			<img src="<?php $this->options->weixinUrl(); ?>" alt="微信打赏" />
                			<?php else: ?>
                			<img src="https://www.hiai.top/yuan/weixinpay.jpg" alt="微信打赏">
                			<?php endif; ?>
						</a>
					</div>
					<div class="aliPay">
						<p>支付宝打赏</p>
						<a href="https://www.hiai.top/yuan/qqpay.jpg"
						data-fancybox="images">
							<?php if (!empty($this->options->alipayUrl)): ?>
                			<img src="<?php $this->options->alipayUrl(); ?>" alt="支付宝打赏" />
                			<?php else: ?>
                			<img src="https://www.hiai.top/yuan/qqpay.jpg"alt="支付宝打赏">
                			<?php endif; ?>
						</a>
					</div>
					<p class="mobile">手机版可截图扫描相册二维码打赏</p>
				</div>
				<i class="fa fa-gift"></i>打赏
			</div>
		</div>
	</div>
	<ul class="post-near">
        <li>上一篇: <?php $this->thePrev('%s','没有了'); ?></li>
        <li>下一篇: <?php $this->theNext('%s','没有了'); ?></li>
    </ul>
	</div>

	<!--发表评论-->
	<?php $this->need('comments.php'); ?>


</div>

<?php $this->need('footer.php'); ?>